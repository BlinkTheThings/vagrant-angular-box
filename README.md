Vagrant Angular Box
=
Vagrant box for developing with Angular based on ubuntu/xenial64.

Creating the Vagrant Angular Box
--
Package and add the box to your system:
````
make package
````

Clean up files generated during packaging:
````
make clean
````

Using the Vagrant Angular Box
--
From a new directory run the following commands to initialize and start the box:
````
vagrant init blinkthethings/angular
vagrant up
````
