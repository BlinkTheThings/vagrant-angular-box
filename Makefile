package: clean
	vagrant up
	vagrant package --output ./angular.box --vagrantfile Vagrantfile.package
	vagrant box add blinkthethings/angular-test ./angular.box

clean:
	vagrant destroy
	vagrant box remove blinkthethings/angular-test --box-version 0 || true
	rm -rf .vagrant
	rm -rf angular.box

.PHONY: package clean
