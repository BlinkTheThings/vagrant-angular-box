# -*- mode: ruby -*-
# vi: set ft=ruby :

$script = <<SCRIPT
#!/bin/bash

# Update apt
apt-get update

# Upgrade packages
apt-get -y upgrade

# Install tools required to build source code
apt-get install -y build-essential libssl-dev autoconf automake libtool pkg-config

# Install tree in case you are following along with ng-book
apt-get install -y tree

# Download and install NodeJS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs

# Update NPM to the latest version
npm install -g npm@latest

# Install TypeScript
npm install -g typescript

# Install Angula CLI
npm install -g @angular/cli

# Clone, build, and install Watchman
git clone https://github.com/facebook/watchman.git
cd watchman
git checkout -b build-v4.9.0 v4.9.0
./autogen.sh
./configure --without-python --without-pcre
make
make install
cd ..
rm -rf watchman

# Clean apt cache
apt-get clean

# Zero out the drive to help make it smaller
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
SCRIPT

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false

  config.vm.box = "ubuntu/xenial64"
  config.vm.box_version = "20180802.0.0"

  config.vm.provider "virtualbox" do |vb|
    vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
  end

  config.vm.provision "shell", inline: $script
end
